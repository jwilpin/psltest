import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsCalculatorImpl implements PrimeFactorsCalculator {

	@Override
	public List<Long> getPrimeFactors(long number) {
		List<Long> primefactors = new ArrayList<>();
		long possiblePrimeFactor = 2;
		while(possiblePrimeFactor <= number) {
			if (number % possiblePrimeFactor == 0) {
				// prime factor...
				primefactors.add(possiblePrimeFactor);
				if(possiblePrimeFactor < number) {
					primefactors.addAll(getPrimeFactors(number/possiblePrimeFactor));
				}
				break;
			}
			possiblePrimeFactor++;
		}
		return primefactors;
	}

}
