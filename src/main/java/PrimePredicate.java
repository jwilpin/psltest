import java.util.function.Predicate;

public class PrimePredicate implements Predicate<Long>{
	
	private PrimeFactorsCalculator primeCalculator;
	
	public PrimePredicate() {
		primeCalculator = new PrimeFactorsCalculatorImpl();
	}
	
	@Override
	public boolean test(Long number) {
		return primeCalculator.getPrimeFactors(number).size() == 1;
	}

}
