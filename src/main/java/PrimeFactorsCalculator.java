import java.util.List;

public interface PrimeFactorsCalculator {
	
	public List<Long> getPrimeFactors(long number);
	
}
