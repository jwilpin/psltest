import java.util.function.Supplier;

public class FibonacciSupplier implements Supplier<Long> {
	
	private FibonacciCalculator fibonacciCalculator;
	private int sequence;
	
	public FibonacciSupplier(){
		sequence = 1;
		fibonacciCalculator = FibonacciCalculatorImpl.getInstance();
	}
	
	public Long get() {
		return fibonacciCalculator.getFibonacciNumber(sequence++);
	}

}
