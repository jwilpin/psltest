public interface FibonacciCalculator {
	
	public long getFibonacciNumber(int sequence);

}
