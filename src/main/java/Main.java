import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// Write code to find the first prime
		// fibonacci number larger than a given minimum. For example, the first
		// prime fibonacci number larger than 10 is 13.

		// Step 1. Use your code to compute the smallest prime fibonacci number
		// greater than 227,000. Call this number X.
		long X = Stream
				.generate(new FibonacciSupplier())
				.filter(new PrimePredicate())
				.filter(primeFibonacci -> primeFibonacci > 227000)
				.findFirst().get();
		
		// Step 2. The answer for Level 2 is the sum of prime divisors of X + 1.
		//
		// For the second portion of this task, note that for the number 12 we consider
		// the sum of the prime divisors to be 2 + 3 = 5. We do not include 2 twice
		// even though it divides 12 twice.
		long sumOfPrimeDivisors = 
				new PrimeFactorsCalculatorImpl().getPrimeFactors(X + 1)
				.stream()
				.distinct()
				.mapToLong(Long::longValue)
				.sum();
		
		System.out.println(String.format(
				"the sum of the prime divisors of %d is %d", 
				X, 
				sumOfPrimeDivisors));
				
	}

}
