import java.util.ArrayList;
import java.util.List;

public class FibonacciCalculatorImpl implements FibonacciCalculator {
	
	private static final FibonacciCalculator instance = new FibonacciCalculatorImpl();
	
	public static FibonacciCalculator getInstance() {
		return instance;
	}
	
	private List<Long> fibonacciSequence;
	
	private FibonacciCalculatorImpl() {
		fibonacciSequence = new ArrayList<>(10000);
		fibonacciSequence.add(0l);// Fibonacci(0)
		fibonacciSequence.add(1l);// Fibonacci(1)
		fibonacciSequence.add(1l);// Fibonacci(2)
	}
	
	@Override
	public long getFibonacciNumber(int sequence) {
		if(sequence < fibonacciSequence.size()) {
			return fibonacciSequence.get(sequence);
		}
		long fibonacci1 = getFibonacciNumber(sequence - 1);
		long fibonacci2 = fibonacciSequence.get(sequence - 2);
		long fibonacci = fibonacci1 + fibonacci2;
		fibonacciSequence.add(sequence, fibonacci);
		return fibonacci;
	}
	

}
