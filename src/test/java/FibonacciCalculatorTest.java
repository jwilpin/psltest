import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FibonacciCalculatorTest {
	
	FibonacciCalculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = FibonacciCalculatorImpl.getInstance();
	}

	@Test
	public void testGetFibonacci() {
		// Arrange
		int fibonacciSequence = 1;
		long expected = 1;
		// Act
		long fibonacci = calculator.getFibonacciNumber(fibonacciSequence);
		// Assert
		assertEquals("fexpected fibonacci of " + fibonacciSequence, expected, fibonacci);
	}
	
	@Test
	public void testGetFibonacci2() {
		// Arrange
		int fibonacciSequence = 2;
		long expected = 1;
		// Act
		long fibonacci = calculator.getFibonacciNumber(fibonacciSequence);
		// Assert
		assertEquals("fexpected fibonacci of " + fibonacciSequence, expected, fibonacci);
	}
	
	@Test
	public void testGetFibonacci3() {
		// Arrange
		int fibonacciSequence = 3;
		long expected = 2;
		// Act
		long fibonacci = calculator.getFibonacciNumber(fibonacciSequence);
		// Assert
		assertEquals("fexpected fibonacci of " + fibonacciSequence, expected, fibonacci);
	}
	
	@Test
	public void testGetFibonacci10() {
		// Arrange
		int fibonacciSequence = 10;
		long expected = 55;
		// Act
		long fibonacci = calculator.getFibonacciNumber(fibonacciSequence);
		// Assert
		assertEquals("fexpected fibonacci of " + fibonacciSequence, expected, fibonacci);
	}
	
}
