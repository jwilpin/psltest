import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PrimeFactorsCalculatorTest {
	
	PrimeFactorsCalculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = new PrimeFactorsCalculatorImpl();
	}

	@Test
	public void test() {
		// Arrange
		long inputNumber = 72;
		List<Long> expectedPrimeFactors = Arrays.asList(2l, 2l, 2l, 3l, 3l);
		// Act
		List<Long> primeFactors = calculator.getPrimeFactors(inputNumber);
		// Assert
		assertEquals("expected prime factors of " + inputNumber, expectedPrimeFactors, primeFactors);
	}
	
	@Test
	public void testPrimeNumber() {
		// Arrange
		long inputNumber = 11;
		List<Long> expectedPrimeFactors = Arrays.asList(inputNumber);
		// Act
		List<Long> primeFactors = calculator.getPrimeFactors(inputNumber);
		// Assert
		assertEquals("expected prime factors of " + inputNumber, expectedPrimeFactors, primeFactors);
	}
	
}
