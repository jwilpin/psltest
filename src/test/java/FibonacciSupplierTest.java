import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

public class FibonacciSupplierTest {
	
	FibonacciSupplier fibonacciSupplier;
	
	@Before
	public void setUp() throws Exception {
		fibonacciSupplier = new FibonacciSupplier();
	}

	@Test
	public void testFibonacciSequenceOf10() {
		// Arrange
		List<Long> fibonacciExpectedSequence = Arrays.asList(1l, 1l, 2l, 3l, 5l, 8l, 13l, 21l, 34l, 55l);
		// Act
		List<Long> fibonacciSequence = 
				Stream
				.generate(fibonacciSupplier)
				.limit(10)
				.collect(Collectors.toList());
		// Assert
		assertEquals("expected fibonacci of 10", fibonacciExpectedSequence, fibonacciSequence);
	}
	
}
