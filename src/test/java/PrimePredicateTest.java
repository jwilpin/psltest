import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PrimePredicateTest {
	
	PrimePredicate primePredicate;
	
	@Before
	public void setUp() throws Exception {
		primePredicate = new PrimePredicate();
	}

	@Test
	public void testWithNoPrimeInput() {
		// Arrange
		boolean isPrimeExpected = false;
		long inputNumber = 20;
		// Act
		boolean isPrime = primePredicate.test(inputNumber);
		// Assert
		assertEquals("expected prime validation of " + inputNumber, isPrimeExpected, isPrime);
	}
	
	@Test
	public void testWithPrimeInput() {
		// Arrange
		boolean isPrimeExpected = true;
		long inputNumber = 23;
		// Act
		boolean isPrime = primePredicate.test(inputNumber);
		// Assert
		assertEquals("expected prime validation of " + inputNumber, isPrimeExpected, isPrime);
	}
	
}
